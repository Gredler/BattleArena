package characters;

import java.util.concurrent.ThreadLocalRandom;

public class Dwarf extends Character 
{
	public Dwarf(String name) 
	{
		super(name);
	}

	@Override
	public void attack(Character enemy) 
	{
		int attackDamage = ThreadLocalRandom.current().nextInt(15, 26);

		if (specialActive) 
		{
			boolean success = false;
			
			if(health <= 10)
			{
				success = headbuttSuccess(0.7);
			}
			else if (health <= 20)
			{
				success = headbuttSuccess(0.5);

			}
			else if (health <= 50)
			{
				success = headbuttSuccess(0.3);
			}
			
			if(success) attackDamage *= 2;
			else attackDamage /= 2;
			
			System.out.println( this.name + " attacks using a headbutt with " + attackDamage + " power!");
		} 
		else
			System.out.println(this.name + " attacks with " + attackDamage + " power!");

		enemy.takeDamage(attackDamage);
	}
	
	private boolean headbuttSuccess(double successRate)
	{
		if (ThreadLocalRandom.current().nextDouble() <= successRate) return true;
		else return false;
	}
}
