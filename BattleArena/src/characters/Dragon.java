package characters;

import java.util.concurrent.ThreadLocalRandom;

public class Dragon extends Character
{
	public Dragon(String name) 
	{
		super(name);
	}

	@Override
	public void attack(Character enemy) 
	{
		int attackDamage = ThreadLocalRandom.current().nextInt(20, 26);
		
		if(specialActive)
		{
			attackDamage -= ThreadLocalRandom.current().nextInt(5, 11);
			System.out.println("The flying " + this.name + " attacks with " + attackDamage + " power!");
		}
		else
			System.out.println(this.name + " attacks with " + attackDamage + " power!");
		
		
		enemy.takeDamage(attackDamage);
	}
	
	public void restoreHealth() 
	{
		if(this.specialActive)
		{
			this.health += 10;
			
			System.out.println(this.name + " restored 10 Health Points from flying.");
		}
	}
}
