package characters;

public abstract class Character 
{
	protected String name;
	protected int health;
	protected boolean specialActive;
	
	public Character(String name) 
	{
		this.name = name;
		this.health = 100;
		this.specialActive = false;
	}
	
	public void takeDamage(int damage)
	{
		health -= damage;
		if(health < 0) health = 0;
	}
	
	public abstract void attack(Character enemy);
	
	public void toggleSpecial()
	{
		if(this.specialActive)
		{
			this.specialActive = false;
			System.out.println(this.name + " has deactivated their special ability!");
		}
		else
		{
			this.specialActive = true;
			System.out.println(this.name + " has activated their special ability!");
		}
	}
	
	public void showStatus()
	{
		System.out.println("Species: " + this.getSpecies());
		System.out.println("Name: " + this.name);
		System.out.println("Health: " + this.health);
		System.out.println("Special: " + ((this.specialActive) ? "active" : "not active"));
	}
	
	private String getSpecies() 
	{
		if(this instanceof Dragon) return "Dragon";
		else if (this instanceof Dwarf) return "Dwarf";
		else return "";
	}

	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	public int getHealth() 
	{
		return health;
	}
	public void setHealth(int health) 
	{
		this.health = health;
	}
	public boolean isSpecialActive() 
	{
		return specialActive;
	}
	public void setSpecialActive(boolean specialActive) 
	{
		this.specialActive = specialActive;
	}
}
