package main;

import java.util.Scanner;

import characters.Character;
import characters.*;

public class MainMenu 
{
	Scanner scan;
	private Character char1, char2;
	private boolean charactersChosen;

	public MainMenu() 
	{
		this.scan = new Scanner(System.in);
		this.charactersChosen = false;
	}

	public void showMenu() 
	{
		System.out.println("MENU");
		System.out.println("1) Choose Characters");
		System.out.println("2) Show Characters");
		System.out.println("3) Fight");
		System.out.println("x) exit");
	}

	public void start() {
		String s = "-";
		while (!s.equals("x")) 
		{
			showMenu();
			s = scan.nextLine();
			switch(s)
			{
				case "1":
					System.out.println("Character 1:");
					char1 = createCharacter(char1);
					System.out.println("============");
					System.out.println("Character 2:");
					char2 = createCharacter(char2);
					
					if(char1 != null && char2 != null) charactersChosen = true;
					else charactersChosen = false;
					
					break;
				case "2":
					if(charactersChosen)
					{
						System.out.println("============");
						System.out.println("Character1: ");
						char1.showStatus();
						System.out.println("============");
						System.out.println("Character2: ");
						char2.showStatus();
						System.out.println();
					}
					else
					{
						charCreationMessage();
					}
	
					break;
				case "3":
					if(charactersChosen)
					{
						Fight fmenu = new Fight(char1, char2);
						fmenu.startTurn();
					}
					else
					{
						charCreationMessage();
					}
				default:
					break;
			}
		}
		scan.close();
	}
	
	private Character createCharacter(Character character)
	{
		System.out.println("1) Choose Dragon");
		System.out.println("2) Choose Dwarf");
		String input = scan.nextLine();

		if(input.equals("1"))
		{
			System.out.print("What's the name of your character? ");

			character = new Dragon(scan.nextLine());
		}
		else if(input.equals("2"))
		{
			System.out.print("What's the name of your character? ");

			character = new Dwarf(scan.nextLine());
		}
		else 
		{
			System.out.println("Wrong Input!");
			return null;
		}
		
		return character;
	}
	
	private void charCreationMessage() 
	{
		System.out.println("Choose your Characters first!");
	}

}
