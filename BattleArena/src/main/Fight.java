package main;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import characters.Character;
import characters.*;

public class Fight
{
	private Scanner scan;
	private Character char1, char2;
	private int turncount;
	
	public Fight(Character char1, Character char2)
	{
		super();
		this.scan = new Scanner(System.in);
		
		this.char1 = char1;
		this.char2 = char2;
		turncount = 1;
	}
	
	private void showMenu(Character charakter)
	{
		System.out.println("1) Attack");
		System.out.println("2) " + (charakter.isSpecialActive() ? "Deactivate " : "Activate ") + "Special Ability");
	}
	
	public void startTurn()
	{
		while(char1.getHealth() > 0 && char2.getHealth() > 0)
		{
			System.out.println("Turn " + turncount);
			System.out.println("=============");
			
			if (ThreadLocalRandom.current().nextDouble() <= 0.5)
			{
				chooseMove(char1, char2);
				if(char2.getHealth() > 0) chooseMove(char2, char1);
			}
			else
			{
				chooseMove(char2, char1);
				if(char1.getHealth() > 0) chooseMove(char1, char2);
			}
			
			if(char1.getHealth() <= 0) 
			{
				deathMessage(char1);
				break;
			}
			if(char2.getHealth() <= 0) 
			{
				deathMessage(char2);
				break;
			}
			
			if(char1 instanceof Dragon) ((Dragon) char1).restoreHealth();
			if(char2 instanceof Dragon) ((Dragon) char2).restoreHealth();
			
			System.out.print("Do you want to see the current stats? ");
			if(scan.nextLine().toLowerCase().equals("yes"))
			{
				System.out.println();
				System.out.println("Character 1:");
				char1.showStatus();
				System.out.println("\nCharacter 2:");
				char2.showStatus();
			}
			
			System.out.println("=============");
			turncount++;
		}
		
		char1.setHealth(100);
		char1.setSpecialActive(false);
		
		char2.setHealth(100);
		char2.setSpecialActive(false);
	}

	public void chooseMove(Character attacker, Character defender)
	{
		System.out.println(attacker.getName() + " is choosing a move.");
		showMenu(attacker);	
		switch(scan.nextLine())
		{
			case "1":
				attacker.attack(defender);
				break;
			case "2":
				attacker.toggleSpecial();
				break;
			default:
				break;
		}
	}
	
	private void deathMessage(Character character) 
	{
		System.out.println(character.getName() + " died from the damage.");
	}
}
